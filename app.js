var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
// var api = require('./routes/api');
var auth = require('./routes/auth/authtoken');
const mongoose = require('mongoose');

var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');
var apiRouter = require('./routes/api');
var login = require('./routes/login');
var registerRouter = require('./routes/register');
// var verifyToken = require('./middleware/jwt_decode');

var app = express();
require("./db")
require('dotenv').config();
// view engine setup

app.use(cors())
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// router.use(bodyParser.json());
// router.use(bodyParser.urlencoded({extended: true}));

app.use('/', indexRouter);
// app.use('/users', usersRouter);
app.use('/api/v1/login', login);  
app.use('/api/v1/register', registerRouter);
app.use('/api/v1',auth, apiRouter);
// app.use('/api/v1', api);
// app.use('/api',verifyToken, apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});




module.exports = app;
