const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// const OrderItemSchema = new Schema({
//   productId: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
//   productName: { type: String, required: true },
//   quantity: { type: Number, required: true },
//   img: { type: String, required: true },
//   price: { type: Number, required: true },
//   total: { type: Number, required: true }
// });

// const OrderUserSchema = new Schema({
//   userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
//   username: { type: String, required: true },
//   userRole: { type: String, required: true }
// });

// const OrderCartSchema = new Schema({
//   cartId: { type: Schema.Types.ObjectId, ref: 'Cart', required: true },
//   items: [OrderItemSchema], // Items specific to this cartId
//   cartTotal: { type: Number, required: true }
// });

// const OrderSchema = new Schema({
//   cartDetails: [OrderCartSchema], // Changed to an array of OrderCartSchema
//   user: OrderUserSchema, // Changed to singular as it's for a single user
//   pay: { type: Number, required: true },
//   paymentMethod: { type: String, required: true },
//   billingAddress: { type: String, required: true },
//   createdAt: { type: Date, default: Date.now },
//   status: { type: String, required: true, default: 'ordered' }
// });

const OrderItemSchema = new Schema({
  productId: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
  productName: { type: String, required: true },
  quantity: { type: Number, required: true },
  img: { type: String, required: true },
  price: { type: Number, required: true },
  total: { type: Number, required: true }
});

const OrderUserSchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  username: { type: String, required: true },
  userRole: { type: String, required: true }
});

const OrderCartSchema = new Schema({
  cartId: { type: Schema.Types.ObjectId, ref: 'Cart', required: true },
  items: [OrderItemSchema], // Items specific to this cartId
  cartTotal: { type: Number, required: true }
});

const OrderSchema = new Schema({
  cartDetails: [OrderCartSchema], // Changed to an array of OrderCartSchema
  user: OrderUserSchema, // Changed to singular as it's for a single user
  pay: { type: Number, required: true },
  paymentMethod: { type: String, required: true },
  billingAddress: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
  status: { type: String, required: true, default: 'ordered' }
});

module.exports = mongoose.model("Order", OrderSchema);
