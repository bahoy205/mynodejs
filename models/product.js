// const mongoose = require("mongoose");
// const products = new mongoose.Schema({
//     productname: {type : String},
//     amount: {type : Number},
//     pirce: { type : Number},
    
// });



// module.exports = mongoose.model("products",products);

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// const typeSchema = new Schema({
//   typeId: { type: Schema.Types.ObjectId, ref: 'Type', required: true },
//   typename: { type: String, required: true },
// });

const productSchema = new Schema({
  productName: { type: String },
  // typeId: { type: Schema.Types.ObjectId, ref: 'Type', required: true },
  // typename: { type: String, required: true },
  amount: { type: Number },
  price: { type: Number },
  img: { type: String },
});


module.exports = mongoose.model("Product", productSchema);
