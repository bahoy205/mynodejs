// const mongoose = require("mongoose");
// const product = require("./product");
// const carts = new mongoose.Schema({
//     productId: {type:mongoose.Types.ObjectId, ref:'products'},
//     pirce: { type : Number},
//     quantity: {type: String},
//     total: { type: Number}
// });



// module.exports = mongoose.model("carts",carts);


//มีการเก็บข้อมูล users
// const mongoose = require('mongoose');
// const Schema = mongoose.Schema;

// const CartItemSchema = new Schema({
//   productId: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
//   quantity: { type: Number, required: true },
//   price: { type: Number, required: true },
//   total: { type: Number, required: true }
// });

// const CartSchema = new Schema({
//   items: [CartItemSchema],
//   userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
//   createdAt: { type: Date, default: Date.now }
// });

// const Cart = mongoose.model('Cart', CartSchema);

// module.exports = Cart;

// const mongoose = require('mongoose');
// const Schema = mongoose.Schema;

// const CartItemSchema = new Schema({
//   productId: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
//   quantity: { type: Number, required: true },
//   price: { type: Number, required: true },
//   total: { type: Number, required: true }
// });

// const CartSchema = new Schema({
//   items: [CartItemSchema],

//   createdAt: { type: Date, default: Date.now }
// });

// module.exports = mongoose.model("carts",CartSchema);

// const mongoose = require('mongoose');
// const Schema = mongoose.Schema;

// const CartItemSchema = new Schema({
//   productId: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
//   productName: { type: String, required: true },
//   quantity: { type: Number, required: true },
//   price: { type: Number, required: true },
//   total: { type: Number, required: true }
// });

// const CartSchema = new Schema({
//   items: [CartItemSchema],
//   createdAt: { type: Date, default: Date.now },
//   status: { type: String, default: 'pending' }
// });

// module.exports = mongoose.model("Cart", CartSchema);

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CartItemSchema = new Schema({
  productId: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
  productName: { type: String, required: true },
  img:{ type: String, require: true},
  quantity: { type: Number, required: true },
  price: { type: Number, required: true },
  total: { type: Number, required: true }
});

const CartSchema = new Schema({
  items: [CartItemSchema],
  userId: { type: Schema.Types.ObjectId, ref: 'User', required: true }, // เพิ่มฟิลด์ userId
  username: { type: String, required: true }, // เพิ่มฟิลด์ username
  userRole: { type: String, required: true }, // เพิ่มฟิลด์ userRole
  createdAt: { type: Date, default: Date.now },
  status: { type: String, default: 'pending' }
});

module.exports = mongoose.model("Cart", CartSchema);