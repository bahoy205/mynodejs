const mongoose = require("mongoose");
const users = new mongoose.Schema({
    firstname: {type : String},
    lastname: {type : String},
    username: { type : String, unique:true },
    password: { type : String},
    role: { type: String },
    Isactive:{ type: Boolean, default:false }
});



module.exports = mongoose.model("users",users);