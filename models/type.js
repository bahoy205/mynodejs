const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// const OrderItemSchema = new Schema({
//   productId: { type: Schema.Types.ObjectId, ref: 'Product', required: true },
//   productName: { type: String, required: true },
//   quantity: { type: Number, required: true },
//   price: { type: Number, required: true },
//   total: { type: Number, required: true }
// });

const TypeSchema = new Schema({
    typename: { type: String},
});

module.exports = mongoose.model("TypeProduct", TypeSchema);