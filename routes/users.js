var express = require('express');
var router = express.Router();
var multer = require('multer')

const bcrypt = require('bcrypt');
const userSchema = require('../models/user.model');
const Users = require("../models/user");
const jwt = require('jsonwebtoken');


/* -------------------- Config Upload file -------------------- */

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/images')
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    cb(null, uniqueSuffix + '_' + file.originalname)
  }
})
const upload = multer({ storage: storage })

// ---------- /users -------------
/* GET users listing. */
router.get('/:id', async function (req, res, next) {
  try {
    let params = req.params
    let querys = req.query

    // let hash = await bcrypt

    let token = await jwt.sign({foo: 'tar'}, '1111',{expiresIn: '1m'})

    let jwt = 

    // let users = await userSchema.find({})

    res.status(200).send(users);
  } catch (error) {
    res.status(500).send(error.toString())
  }
});

/* POST */
router.post('/', upload.single('profile'), async function (req, res, next) {
  try {
    let { name, age } = req.body

    let user = new userSchema({
      name,
      age,
      file: req.file.filename
    })

    let save = await user.save()

    res.status(200).send(save);
  } catch (error) {
    res.status(500).send(error.toString())
  }
});


/* PUT */
router.put('/:id', async function (req, res, next) {
  try {
    let { name, age } = req.body

    let update = await userSchema.findByIdAndUpdate(req.params.id, { name, age }, { new: true })

    res.status(200).send(update);
  } catch (error) {
    res.status(500).send(error.toString())
  }

});

/* DELETE */
router.delete('/:id', async function (req, res, next) {
  try {
    
    let delete_user = await userSchema.findByIdAndDelete(req.params.id)

    res.status(200).send(delete_user);
  } catch (error) {
    res.status(500).send(error.toString())
  }

});

/* Register */
router.post("/v1/register",async function (req, res, next){
  try{
    let { password, username, firtname, lastname, role } = req.body;
    let hashPassword = await bcrypt.hash(password,10);
  
    const newUser = new Users({
      firstname: firtname,
      lastname: lastname,
      username: username,
      password: hashPassword,
      role: role,
    });
    console.log(newUser);
    const user = await newUser.save();
    console.log(user);
    return res.status(200).send({
      data: {_id: user._id,username,firtname,lastname,role},
      message: "create success",
      success: true,
    });
  } catch (error) {
    return res.status(500).send({
      message: "create fail",
      success: false,
    });
  }
});

router.post('/v1/login', async function(req, res, next) {
  try {
    let { username, password } = req.body;

    // Check if both username and password are provided
    if (username && password) {
      return res.status(400).send("Both username and password are required");
    }

    // Find user by username
    const user = await Users.findOne({ username });

    // Check if user exists and password is correct
    if (user && await bcrypt.compare(password, user.password)) {
      // Create JWT token
      const token = jwt.sign(
        { user_id: user._id, username },
        process.env.TOKEN_KEY,
        { expiresIn: "2h" }
      );

      // Attach token to user object
      user.token = token;

      // Return user object with token
      return res.status(200).json(user);
    }

    // If username or password is incorrect, return error message
    return res.status(400).send("Invalid credentials");
  } catch (error) {
    console.log(error);
    // Handle other errors
    return res.status(500).send("Internal Server Error");
  }
});
module.exports = router;
