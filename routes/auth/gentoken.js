const mongoose = require('mongoose');
const Users = require('../../models/users');
const jwt = require('jsonwebtoken');

async function login(username, password) {
  try {
    let user = await Users.findOne({ username: username });
    if (user) {
      if (user.isActive) {
        // Use bcrypt to compare passwords
        const checkPassword = await bcrypt.compare(password, user.password);
        if (checkPassword) {
          // Generate JWT token upon successful login
          const token = jwt.sign({ id: user._id, username: user.username, role: user.role }, process.env.SECRETTKEY, { expiresIn: '30m', algorithm: 'HS256' });
          return { success: true, token: token, role: user.role };
        } else {
          return { success: false, message: "Incorrect password" };
        }
      } else {
        return { success: false, message: "User is inactive" };
      }
    } else {
      return { success: false, message: "User not found" };
    }
  } catch (error) {
    console.error('Login failed:', error);
    return { success: false, message: "Internal server error" };
  }
}

module.exports = login;