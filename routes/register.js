var express = require('express');
var router = express.Router();
var bcrypt= require('bcrypt');

const Users = require("../models/user");



router.post("/", async function (req, res, next) {
    try {
      let { password, username, firstname, lastname , role} = req.body;
      let hashPassword = await bcrypt.hash(password, 10);
        // console.log()
      const newUser = new Users({
        firstname: firstname,
        lastname: lastname,
        username: username,
        password: hashPassword,
        role: role, 
      });
    //   console.log();
      
       await newUser.save();
      
      return res.status(200).send({
        data: newUser,
        message: "create success",
        success: true,
      });
    } catch (error) {
      return res.status(404).send({
        message: "create fail",
        success: false,
      });
    }
  });

  module.exports = router;