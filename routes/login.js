const express = require("express");
const router = express.Router();
const Users = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

router.post("/", async function (req, res, next) {
  try {
    const { password, username } = req.body;
    const user = await Users.findOne({ username });

    if (!user) {
      return res.status(404).send({
        message: "User not found",
        success: false,
      });
    }

    if (!user.Isactive) {
      return res.status(403).send({
        message: "Your account is pending approval. You cannot log in yet.",
        success: false,
      });
    }

    const checkPassword = await bcrypt.compare(password, user.password);
    if (!checkPassword) {
      return res.status(401).send({
        message: "Incorrect password",
        success: false,
      });
    }

    let role;
    if (user.role === 'admin') {
      role = 'admin';
    } else {
      role = 'employee';
    }

    const token = jwt.sign(
      { id: user._id, username: user.username, role: user.role },
      process.env.SECRETTKEY,
      { expiresIn: "30m", algorithm: "HS256" }
    );

    // const { _id, firstname, lastname, role } = user;
    return res.status(201).send({
      token: token,
      message: "Login successful",
      success: true,
    });
  } catch (error) {
    console.error("Login failed:", error);
    return res.status(500).send({
      message: "Login failed",
      success: false,
    });
  }
});

// router.get("/protected", Auth, (req, res) => {
//   res.status(200).send({
//     message: "You accessed a protected route",
//     user: { id: req.uid, username: req.username, role: req.role },
//   });
// });

// /* GET home page. */
// router.get("/", function (req, res, next) {
//   res.render("index", { title: "Express" });
// });

module.exports = router;
