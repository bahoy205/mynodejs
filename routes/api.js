var express = require("express");
var router = express.Router();
var multer = require("multer");
var url = "mongodb://127.0.0.1:27017/";
var axios = require("axios");

const Users = require("../models/user");
const Products = require("../models/product");
const Orders = require("../models/order");
const Carts = require("../models/cart");
// const Types = require("../models/type");
const mongoose = require("mongoose");
const Auth = require("./auth/authtoken");
const QRCode = require("qrcode");
const generatePayload = require("promptpay-qr");
const puppeteer = require('puppeteer');
// const  PHONENUMBER  = process.env.PHONENUMBER;
// const fs = require("fs");
// const path = require("path");

// const cart = require("../models/cart");

/* -------------------- Config Upload file -------------------- */

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/images");
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    cb(null, uniqueSuffix + "_" + file.originalname);
  },
});
const upload = multer({ storage: storage });

/* DELETE */
router.delete("/users/:id", async function (req, res, next) {
  try {
    let delete_user = await Users.findByIdAndDelete(req.params.id);

    res.status(200).send(delete_user);
  } catch (error) {
    res.status(500).send(error.toString());
  }
});

/* Update User */
router.put("/approve/:id", async function (req, res, next) {
  try {
    // let { role } = req.body
    // console.log(hee);
    let id = req.params.id;
    console.log(id);
    let update = await Users.findByIdAndUpdate(id, { Isactive: true });

    res.status(200).send({
      data: update,
      message: "Approve success",
      success: true,
    });
  } catch (error) {
    res.status(500).send(error.toString());
  }
});

router.get("/products/flutter/:id", async function (req, res) {
  try {
    const typeId = req.params.id;

    // Find products by typeId
    const products = await Products.find({ typeId });

    if (products.length === 0) {
      return res.status(404).send({
        message: "No products found for the given typeId.",
        success: false,
      });
    }

    return res.status(200).send({
      data: products,
      message: "Products retrieved successfully",
      success: true,
    });
  } catch (error) {
    console.error("Error retrieving products:", error);
    return res.status(500).send({
      message: "Failed to retrieve products",
      success: false,
      error: error.message,
    });
  }
});

router.post("/products", async function (req, res, next) {
  try {
    let { productName, amount, price, img, 
     
    } = req.body;

    
    const existingProduct = await Products.findOne({
      productName: productName,
    });
    if (existingProduct) {
      return res.status(400).send({
        message: "This item is repeated.",
        success: false,
      });
    }

    const newProduct = new Products({
      productName: productName,
      amount: amount,
      price: price,
      img: img,
    });

    const product = await newProduct.save();

    return res.status(200).send({
      data: {
        _id: product._id,
        productName,
        amount,
        price,
      },
      message: "Create success",
      success: true,
    });
  } catch (error) {
    console.error("Error creating product:", error); // Log the error for debugging
    return res.status(500).send({
      message: "Create fail",
      success: false,
      error: error.message, // Include error message in response for debugging
    });
  }
});

/* Update Products */
router.put("/products/:id", async function (req, res, next) {
  try {
    let { productName, amount, price ,img } = req.body;

    let update = await Products.findByIdAndUpdate(req.params.id, {
      productName,
      amount,
      price,
      img,
    });

    res.status(200).send({
      data: update,
      message: "Update success",
      success: true,
    });
  } catch (error) {
    return res.status(500).send({
      message: "Update fail",
      success: false,
    });
  }
});

/* Delete Product */
router.delete("/products/:id", async function (req, res, next) {
  try {
    let id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "Invalid ID",
        success: false,
        error: ["ID is not a valid ObjectId"],
      });
    }

    let deletedProduct = await Products.findByIdAndDelete(id);

    if (!deletedProduct) {
      return res.status(404).send({
        message: "Product not found",
        success: false,
      });
    }

    return res.status(200).send({
      data: deletedProduct,
      message: "Delete Success",
      success: true,
    });
  } catch (error) {
    return res.status(500).send({
      message: "Delete Fail",
      success: false,
      error: error.message, // Send the actual error message
    });
  }
});

/* Get By Products */
router.get("/products/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    console.log(req.params.id);

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "id Invalid",
        success: false,
        error: ["id is not a valid ObjectId"],
      });
    }

    let product = await Products.findById(id);

    if (!product) {
      return res.status(404).send({
        message: "Product not found",
        success: false,
      });
    }

    return res.status(200).send({
      data: product,
      message: "success",
      success: true,
    });
  } catch (error) {
    return res.status(500).send({
      message: "server error",
      success: false,
    });
  }
});

/* Get All Product */
router.get("/products", async function (req, res, next) {
  try {
    let product = await Products.find({});

    res.status(200).send(product);
  } catch (error) {
    res.status(500).send(error.toString());
  }
});

router.get("/users", async function (req, res, next) {
  try {
    let users = await Users.find({});

    res.status(200).send(users);
  } catch (error) {
    res.status(500).send(error.toString());
  }
});

/* Add Oders Product */
router.post("/products/:id/orders", async function (req, res, next) {
  try {
    let { quantity } = req.body;
    const productId = req.params.id;

    const existingProduct = await Products.findById(productId);
    console.log(productId);
    if (!existingProduct) {
      return res.status(404).send({
        message: "The product the customer order was not found.",
        success: false,
      });
    }

    if (existingProduct.amount < quantity) {
      return res.status(400).send({
        message: "Insufficient quantity of products in stock",
        success: false,
      });
    }
    const total = quantity * existingProduct.price;

    const newOrder = new Orders({
      productId: productId,
      quantity: quantity,
      price: existingProduct.price,
      total: total,
      // amount: existingProduct.amount,
    });

    // console.log(order);
    const order = await newOrder.save();

    existingProduct.amount -= quantity;
    await existingProduct.save();

    return res.status(201).send({
      data: order,
      message: "ceate order success",
      message2: "Remaining amount : " + existingProduct.amount,
      success: true,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).send({
      message: "There was an error creating the Order.",
      success: false,
    });
  }
});

/*Get By Id*/
router.get("/orders/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    console.log(req.params.id);

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "id Invalid",
        success: false,
        error: ["id is not a valid ObjectId"],
      });
    }

    let order = await Orders.findById(id);

    if (!order) {
      return res.status(404).send({
        message: "Product not found",
        success: false,
      });
    }

    return res.status(200).send({
      data: order,
      message: "success",
      success: true,
    });
  } catch (error) {
    return res.status(500).send({
      message: "server error",
      success: false,
    });
  }
});

/*GET ALL Order */
router.get("/orders", async function (req, res, next) {
  try {
    let orders = await Orders.find({});

    res.status(200).send(orders);
  } catch (error) {
    res.status(500).send(error.toString());
  }
});

/* Get by Order by Products */
router.get("/products/:id/orders", async function (req, res, next) {
  try {
    let productId = req.params.id;
    let orders = await Orders.find({ productId: productId })
      .populate("productId")
      .exec();

    res.status(200).send(orders);
  } catch (error) {
    res.status(500).send(error.toString());
  }
});

/* Add Product Cart */
router.post("/products/cart", Auth, async function (req, res, next) {
  try {
    const { productId, quantity } = req.body;
    const { id: userId, username, role: userRole } = req.userData;

    // Validate productId and quantity
    if (!productId || !quantity || quantity <= 0) {
      return res.status(400).json({
        message: "Please provide productId and valid quantity",
        success: false,
      });
    }

    // Check if product exists
    const existingProduct = await Products.findById(productId);
    if (!existingProduct) {
      return res.status(404).json({
        message: `Product with ID ${productId} not found`,
        success: false,
      });
    }

    // Check if the product has enough quantity
    if (existingProduct.amount < quantity) {
      return res.status(400).json({
        message: `Insufficient quantity for product with ID ${productId}`,
        success: false,
      });
    }

    // Create cart item
    const total = quantity * existingProduct.price;
    const newCartItem = {
      productId: productId,
      productName: existingProduct.productName,
      img: existingProduct.img,
      quantity: quantity,
      // typename: typename,
      price: existingProduct.price,
      total: total,
    };

    // Update product stock
    existingProduct.amount -= quantity;
    await existingProduct.save();

    // Create and save the cart
    const newCart = new Carts({
      items: [newCartItem],
      totalPrice: total,
      userId: userId,
      username: username,
      userRole: userRole,
    });
    const cart = await newCart.save();

    return res.status(201).json({
      data: {
        cart: cart,
        user: {
          userId: userId,
          username: username,
          userRole: userRole,
        },
      },
      message: "Cart created successfully",
      success: true,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      message: "Failed to create cart",
      success: false,
    });
  }
});

router.delete("/cart/:id", async function (req, res, next) {
  try {
    let id = req.params.id;

    let deletedCart = await Carts.findByIdAndDelete(id);

    if (!deletedCart) {
      return res.status(404).send({
        message: "Product not found",
        success: false,
      });
    }

    return res.status(200).send({
      data: deletedCart,
      message: "Delete Success",
      success: true,
    });
  } catch (error) {
    return res.status(500).send({
      message: "Delete Fail",
      success: false,
      error: error.message, // Send the actual error message
    });
  }
});

router.delete("/wanting", async (req, res) => {
  try {
    await Carts.deleteMany({});
    res.status(200).send({ message: "All products deleted" });
  } catch (err) {
    res.status(500).send({ error: "Failed to delete products" });
  }
});

router.put("/cart/:productId", Auth, async function (req, res, next) {
  try {
    if (!req.userData) {
      return res.status(401).send("Unauthorized");
    }

    const userId = req.userData.id;
    const { productId } = req.params;
    const { quantity } = req.body;

    let cart = await Carts.findOne({ userId: userId, productId: productId });

    if (cart) {
      // Update the quantity of the item in the cart
      cart.quantity += quantity;
    } else {
      // Add the item to the cart
      cart = new Carts({
        userId: userId,
        productId: productId,
        quantity: quantity,
      });
    }

    await cart.save();

    res.status(200).send(cart);
  } catch (error) {
    console.error("Failed to update cart:", error);
    res.status(500).send(error.toString());
  }
});

/* GET CART */
router.get("/cart", Auth, async function (req, res, next) {
  try {
    if (!req.userData) {
      return res.status(401).send("Unauthorized");
    }

    const userId = req.userData.id;

    const cart = await Carts.find({ userId: userId });

    res.status(200).send(cart); 
  } catch (error) {
    console.error("Failed to fetch cart data:", error);
    res.status(500).send(error.toString());
  }
});

router.get("/cart/:id", Auth, async function (req, res, next) {
  try {
    const cartId = req.params.id; // ใช้ req.params.id เพื่อดึง ID จาก URL

    const cart = await Carts.findById(cartId); // ใช้ findById แทนการใช้ find โดยใช้ ID ตรง

    if (!cart) {
      // ตรวจสอบว่ามีข้อมูลตะกร้าหรือไม่
      return res.status(404).send("Cart not found");
    }

    res.status(200).send(cart);
  } catch (error) {
    console.error("Failed to fetch cart data:", error);
    res.status(500).send(error.toString());
  }
});

/*Order User */
router.get("/users/orders", Auth, async (req, res) => {
  try {
    // Extract userId from req.userData
    const userId = req.userData.id;
    console.log(userId);

    // Find orders by userId
    const orders = await Orders.find({ "user.userId": userId });

    // If no orders are found, send a 404 response
    if (orders.length === 0) {
      return res.status(404).send({
        message: "No orders found for the given userId.",
        success: false,
      });
    }

    // If orders are found, send a 200 response with the orders data
    return res.status(200).send({
      data: orders,
      message: "Orders retrieved successfully",
      success: true,
    });
  } catch (error) {
    // Log the error and send a 500 response
    console.error("Error retrieving orders:", error);
    return res.status(500).send({
      message: "Failed to retrieve orders",
      success: false,
      error: error.message,
    });
  }
});

/* Generate To PDF */
router.get("/users/orders/pdf", Auth, async (req, res) => {
  try {
    // Extract userId from req.userData
    const userId = req.userData.id;
    console.log(userId);

    // Find orders by userId
    const orders = await Orders.find({ "user.userId": userId });

    // If no orders are found, send a 404 response
    if (orders.length === 0) {
      return res.status(404).send({
        message: "No orders found for the given userId.",
        success: false,
      });
    }

    // Generate HTML content for the PDF
    const htmlContent = `
      <html>
        <head>
          <style>
            body { font-family: Arial, sans-serif; }
            table { width: 100%; border-collapse: collapse; }
            th, td { padding: 8px; text-align: left; border-bottom: 1px solid #ddd; }
            th { background-color: #f2f2f2; }
            .order-details { margin-bottom: 20px; }
          </style>  
        </head>
        <body>
          <h1>Order Details</h1>
          ${orders.map(order => `
            <div class="order-details">
              <h2>Order ID: ${order._id}</h2>
              <p><strong>Username:</strong> ${order.user.username}</p>
              <p><strong>Billing Address:</strong> ${order.billingAddress}</p>
              <p><strong>Payment Method:</strong> ${order.paymentMethod}</p>
              <p><strong>Order Status:</strong> ${order.status}</p>
              <p><strong>Order Created At:</strong> ${new Date(order.createdAt).toLocaleString()}</p>
              <table>
                <tr>
                  <th>Cart ID</th>
                  <th>Product</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Total</th>
                </tr>
                ${order.cartDetails.map(cart => `
                  ${cart.items.map(item => `
                    <tr>
                      <td>${cart.cartId}</td>
                      <td>${item.productName} <img src="${item.img}" alt="${item.productName}" width="50"></td>
                      <td>${item.quantity}</td>
                      <td>${item.price}</td>
                      <td>${item.total}</td>
                    </tr>
                  `).join('')}
                  <tr>
                    <td colspan="4" style="text-align: right;"><strong>Cart Total:</strong></td>
                    <td>${cart.cartTotal}</td>
                  </tr>
                `).join('')}
              </table>
            </div>
          `).join('')}
        </body>
      </html>
    `;

    // Launch puppeteer browser
    const browser = await puppeteer.launch({ 
      headless: true, 
      args: ['--no-sandbox', '--disable-setuid-sandbox'] 
    });
    const page = await browser.newPage();
    await page.setContent(htmlContent);
    const pdfBuffer = await page.pdf({ format: 'A4' });
    await browser.close();

    // Send the PDF as a response
    res.set({
      'Content-Type': 'application/pdf',
      'Content-Disposition': 'attachment; filename=orders.pdf',
    });
    res.send(pdfBuffer);
  } catch (error) {
    console.error(error);
    res.status(500).send({
      message: "An error occurred while generating the PDF.",
      success: false,
    });
  }
});


/* Order */
// router.post("/orders", Auth, async (req, res) => {
//   try {
//     const { cartIds, invoiceDetails } = req.body;
//     const { id: userId, username, role: userRole } = req.userData;

//     // Validate cartIds
//     if (!Array.isArray(cartIds) || cartIds.length === 0) {
//       return res.status(400).send({
//         message: "cartIds must be a non-empty array",
//         success: false,
//       });
//     }

//     const { pay, paymentMethod, billingAddress } = invoiceDetails;

//     // Validate invoiceDetails
//     if (!pay || !paymentMethod || !billingAddress) {
//       return res.status(400).send({
//         message: "pay, paymentMethod, and billingAddress are required in invoiceDetails",
//         success: false,
//       });
//     }

//     let total = 0;
//     const cartDetails = [];

//     for (const cartId of cartIds) {
//       const cart = await Carts.findById(cartId).populate("items.productId");
//       if (!cart) {
//         return res.status(404).send({
//           message: `Cart not found for ID: ${cartId}`,
//           success: false,
//         });
//       }

//       if (cart.status !== "pending" && cart.status !== "not ordered") {
//         return res.status(400).send({
//           message: `Cannot create order for cart ID: ${cartId}. Cart status is not valid.`,
//           success: false,
//         });
//       }

//       const cartTotal = cart.items.reduce(
//         (sum, item) => sum + item.price * item.quantity,
//         0
//       );
//       total += cartTotal;

//       const items = cart.items.map(item => ({
//         productId: item.productId._id,
//         productName: item.productId.productName,
//         img: item.productId.img,
//         quantity: item.quantity,
//         price: item.price,
//         total: item.price * item.quantity,
//       }));

//       cartDetails.push({ cartId, items, cartTotal });

//       cart.status = "billed";
//       await cart.save();
//     }

//     if (pay < total) {
//       return res.status(400).send({
//         message: `Insufficient payment amount. Total amount is ${total}`,
//         success: false,
//       });
//     }

//     const newOrder = new Orders({
//       cartDetails, // store array of cart details
//       user: {
//         userId,
//         username,
//         userRole,
//       },
//       pay: total, // total pay for all carts
//       paymentMethod,
//       billingAddress,
//       status: 'ordered', // Setting default status explicitly
//     });

//     const savedOrder = await newOrder.save();

//     const change = pay - total;

//     return res.status(201).send({
//       data: {
//         order: {
//           _id: savedOrder._id,
//           cartDetails: savedOrder.cartDetails.map(cartDetail => ({
//             cartId: cartDetail.cartId,
//             items: cartDetail.items.map(item => ({
//               productName: item.productName,
//               quantity: item.quantity,
//               price: item.price,
//               total: item.total,
//             })),
//             cartTotal: cartDetail.cartTotal,
//           })),
//           pay: savedOrder.pay,
//           paymentMethod: savedOrder.paymentMethod,
//           billingAddress: savedOrder.billingAddress,
//           status: savedOrder.status,
//         },
//         user: {
//           userId,
//           username,
//           userRole,
//         },
//       },
//       message: "Order created successfully",
//       message1: `Total amount is ${total}, change is ${change}`,
//       success: true,
//     });
//   } catch (error) {
//     console.error("Error creating order:", error);
//     return res.status(500).send({
//       message: "Error creating order",
//       success: false,
//       error: error.message,
//     });
//   }
// });

router.post("/orders", Auth, async (req, res) => {
  try {
    const { cartIds, invoiceDetails } = req.body;
    const { id: userId, username, role: userRole } = req.userData;

    // Validate cartIds
    if (!Array.isArray(cartIds) || cartIds.length === 0) {
      return res.status(400).send({
        message: "cartIds must be a non-empty array",
        success: false,
      });
    }

    const { paymentMethod, billingAddress } = invoiceDetails;

    // Validate invoiceDetails
    if (!paymentMethod || !billingAddress) {
      return res.status(400).send({
        message: "paymentMethod and billingAddress are required in invoiceDetails",
        success: false,
      });
    }

    let total = 0;
    const cartDetails = [];

    for (const cartId of cartIds) {
      const cart = await Carts.findById(cartId).populate("items.productId");
      if (!cart) {
        return res.status(404).send({
          message: `Cart not found for ID: ${cartId}`,
          success: false,
        });
      }

      if (cart.status !== "pending" && cart.status !== "not ordered") {
        return res.status(400).send({
          message: `Cannot create order for cart ID: ${cartId}. Cart status is not valid.`,
          success: false,
        });
      }

      const cartTotal = cart.items.reduce(
        (sum, item) => sum + item.price * item.quantity,
        0
      );
      total += cartTotal;

      const items = cart.items.map(item => ({
        productId: item.productId._id,
        productName: item.productId.productName,
        img: item.productId.img,
        quantity: item.quantity,
        price: item.price,
        total: item.price * item.quantity,
      }));

      cartDetails.push({ cartId, items, cartTotal });

      cart.status = "billed";
      await cart.save();
    }

    const newOrder = new Orders({
      cartDetails, // store array of cart details
      user: {
        userId,
        username,
        userRole,
      },
      pay: total, // total pay for all carts
      paymentMethod,
      billingAddress,
      status: 'ordered', // Setting default status explicitly
    });

    const savedOrder = await newOrder.save();

    // Generate QR code
    let qrCode = null;
    try {
      qrCode = await router.generateQrcode(savedOrder._id); // Pass orderId to generate QR code
    } catch (err) {
      console.error("Error generating QR code:", err);
      return res.status(500).send({
        message: "Error generating QR code",
        success: false,
        error: err.message,
      });
    }

    return res.status(201).send({
      data: {
        order: {
          _id: savedOrder._id,
          cartDetails: savedOrder.cartDetails.map(cartDetail => ({
            cartId: cartDetail.cartId,
            items: cartDetail.items.map(item => ({
              productName: item.productName,
              quantity: item.quantity,
              price: item.price,
              total: item.total,
            })),
            cartTotal: cartDetail.cartTotal,
          })),
          pay: savedOrder.pay,
          paymentMethod: savedOrder.paymentMethod,
          billingAddress: savedOrder.billingAddress,
          status: savedOrder.status,
        },
        user: {
          userId,
          username,
          userRole,
        },
        qrCode,
      },
      message: "Order created successfully",
      success: true,
    });
  } catch (error) {
    console.error("Error creating order:", error);
    return res.status(500).send({
      message: "Error creating order",
      success: false,
      error: error.message,
    });
  }
});


router.generateQrcode = async (orderId) => {
  try {
    const phoneNumber = process.env.PHONENUMBER;
    if (!phoneNumber) {
      throw new Error("PHONENUM environment variable is not set");
    }

    // Retrieve the order from the database
    const order = await Orders.findById(orderId);
    if (!order) {
      return res.status(404).send({
        message: "Order not found",
        success: false,
      });
    }

    // Extract pay value from the order
    const orderPay = order.pay;

    const payload = generatePayload(phoneNumber, { amount: orderPay });
    const base64String = await QRCode.toDataURL(payload);
    return base64String;
  } catch (err) {
    console.error(err.message);
    throw new Error("Error generating QR code: " + err.message);
  }
}

router.get("/qr", Auth, async function (req, res, next) {
  try {
    const phoneNumber = process.env.PHONENUMBER;
    if (!phoneNumber) {
      throw new Error("PHONENUM environment variable is not set");
    }

    const orderId = req.params.orderId;

    // Retrieve the order from the database
    const order = await Orders.findById(orderId);
    if (!order) {
      return res.status(404).send({
        message: "Order not found",
        success: false,
      });
    }

    // Extract pay value from the order
    const pay = order.pay;

    const payload = generatePayload(phoneNumber, { amount: pay });
    const base64String = await QRCode.toDataURL(payload);
    res.status(200).send({
      qrCode: base64String,
      success: true,
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send({
      message: "Error generating QR code",
      success: false,
      error: err.message,
    });
  }
});

exports.payment = async (data) => {
  console.log("Data: ", data);

  let pricetopay = 0;
  data.products.forEach((productItem) => {
    pricetopay += productItem.quantity * productItem.product.price;
  });

  console.log("Total price to pay: ", pricetopay);

  try {
    const qrCode = await QRcode.generateQrcode(pricetopay);
    return qrCode;
  } catch (error) {
    console.error("Error generating QR code: ", error);
    throw error;
  }
};

/* API GOLD */
// router.get("/gold", async (req, res) => {
//   try {
//     const exchangeRateUSDToTHB = 36.66;
//     const response = [
//       //ประกาศ response มาเป็น array เพื่อเก็บ axios ของแต่ละ api
//       axios.get("https://api.gold-api.com/price/XAU"),
//       axios.get("https://api.gold-api.com/price/XAG"),
//       axios.get("https://api.gold-api.com/price/BTC"),
//       axios.get("https://api.gold-api.com/price/ETH"),
//     ];

//     const responses = await Promise.all(response); // จากนั้นก็นำไป Promist เพื่อเรียกใช้งาน api ทุกตัวที่อยู่ใน response

//     const responseData = responses.map((response) => {
//       //จากนั้นก็ใช้ฟังชั่น map เพื่อแยก Data ออกจาก object
//       const data = response.data;
//       data.price = data.price * exchangeRateUSDToTHB; // แปลงราคาจาก USD เป็น THB
//       return data;
//     });

//     const sortedData = responseData.sort((a, b) => b.price - a.price); //นำขอมูลที่แยกออกมา มาจัดเรียงจากมากไปน้อยด้วยฟังชั่น sort

//     res.json(sortedData);
//   } catch (error) {
//     console.error(error);
//     res.status(500).send("Something went wrong");
//   }
// });



module.exports = router;
