// let num = 10
// let str = '${num}'

// console.log(str)
//unit 7
// let arr = [1,2,3,4]

// arr.push(5)
// arr.pop()

// arr.unshift(0)
// arr.shift()

// // console.log(arr[1])

// let new_arr = arr.map((val, i, arr) => {
//     return val + 1
// })
// // console.log(new_arr)

// let filter_arr =  arr.filter(val => {
//     return val  > 3
// })
// console.log(filter_arr)

//unit 8

// let arr1 = [1,2]
// let arr2 = [3,4]

// console.log([...arr1, ...arr2])

// let obj = {
//     name : 'tar',
//     age : 22
// }
// console.log(obj);
// console.log(obj);

//unit 9

// let time1 = () => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             resolve(console.log('time1'))
//         }, 1000);
//     })
// }

// let time2 = () => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             reject(console.log('time2'))
//         }, 2000);
//     })
// }


// let show = async () => {
//     try {
//         await time2()
//         await time1()
//     } catch (error) {
//         console.log(error)
//     }
// }

// show()   


// console.log("เริ่มการทำงาน")
// setTimeout(() => {
//     console.log("กำลังดาวน์โหลด")
// },3000);
// console.log("จบการทำงาน")

//ทบทวน CallBack 

// function calculate(x,y,callback){
//     console.log("กำลังคำนวณ....")
//     setTimeout(() => {
//         const sum =  x+y
//         callback(sum)
//     },3000)
// }

// //ฟังก์ชั่นปกติ
// // const sum = calculate(100,50)
// // display(sum)
// //ฟังก์ชั่นแบบ callback 
// calculate(100,50,function(result){
//     console.log(`ผลบวก = ${result}`)
//     console.log("คำนวนเสร็จสิ้น")
// })

//เขียนโปรแกรมดาวน์โหลดไฟล์
// const url1 = "tar.dev/file1.json"
// const url2 = "tar.dev/file2.json"
// const url3 = "tar.dev/file3.json"

// function downlonding(url,callback){
//     console.log(`กำลังดาวน์โหลดจาก ${url}`)
//     setTimeout(() => {
//         callback(url)
//     },3000)
    
// }
// downlonding(url1,function(result){
//     console.log(`ดาวน์โหลด ${result} เรียบร้อย!`) 
//     downlonding(url2,function(result){
//         console.log(`ดาวน์โหลด ${result} เรียบร้อย!`) 
//         downlonding(url3,function(result){
//             console.log(`ดาวน์โหลด ${result} เรียบร้อย!`)
//         })
//     })
// })

//สร้าง Promise
const connect = true
const url1 = "tar.dev/file1.json"
const url2 = "tar.dev/file2.json"
const url3 = "tar.dev/file3.json"
function downlonding(url){
    console.log(`กำลังโหลดข้อมูล ${url} `)
    return new Promise(function(resolve,reject){
        setTimeout(() => {
            if(connect){
                resolve(`โหลด ${url} เรียบร้อย`)
            }else{
                reject(`เกิดข้อผิดพลาด`)
            }
        },3000);
    })
}
//Promise Then
// downlonding(url1).then(function(result){
//     console.log(result)
//     return downlonding(url2)
// }).then(function(result){
//     console.log(result)
//     return downlonding(url3)
// }).then(function(result){
//     console.log(result)
// })
//Promise Hell 
// downlonding(url1).then(function(result){
//     console.log(result)
//     downlonding(url2).then(function(result){
//         console.log(result)
//         downlonding(url3).then(function(result){
//             console.log(result)
            
//         })
//     })
// })

// downlonding(url1).then(result =>{
//     console.log(result)
// }).catch(err=>{
//     console.log(err)
// }).finally(() => {
//     console.log("จบการทำงาน")
// })

//Async & Await

async function start(){
    console.log(await downlonding(url1))
    console.log(await downlonding(url2))
    console.log(await downlonding(url3))
}

start()
//unit 12

